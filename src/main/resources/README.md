# Minecraft Electrical Age
### Auteur : Axel Bonvin | Theo Jacquemettaz


Le projet MinecraftController permet la gestion, le pilotage, et le stockage de données, d'un micro-grid dans un monde Minecraft.
Plus spécifiquement, le but de ce projet est de gérer un réseau électrique se trouvant dans un monde Minecraft (grâce au mode Electrical-Age). Ce dernier contient des producteurs (éolienne, panneau solaire, usine à charbon) mais aussi des consommateurs fixes "aléatoires" (bunker, maison et extérieur) et un consommateur fixe (la factory).

Notre objectif est de gérer ce réseau, tout en maximisant la production de notre factory et en minimisant notre consommation de charbon. Pour cela, nous avons développé un code en Java, permettant de faire l'interface entre nos différents outils. Le code contient aussi un "SmartControl", qui est chargé de réguler le microgrid lorsque celui-ci n'est pas géré en manuel.

En dehors de Minecraft, le code Java (qui communique avec le GridMinecraft en Modbus TCP/IP) permet aussi d'assurer la communication entre le micro-grid Minecraft et différents outils en dehors du jeu, comme une base de données (Influx DB) (communication HTTP) et une interface Web de pilotage (communication via WebSocket).

## La structure de notre code :
Le code est composé des classes suivantes :

- **MinecraftController**

    => Contient le "Main" de notre programme. Sert d'interface au code et appelle les différentes classes.

- **SmartControl**

    => Est en charge de réguler le GridMinecraft. Met en priorité l'état de la batterie, qui est un indice de l'état de notre grid, car la tension du grid dépend directement de la batterie. Nos deux paramètres pour gérer le réseau sont :

  1. Modifier la production d'electricite via l'usine a charbon ;

  2. Modifier la consommation d'electricite via la Factory ;
    Cela nous permet de garder un etat de batterie le plus stable possible.
  

- **DataPoint**

    => Le DataPoint est un élément qui correspond à une variable dans le GridMinecraft. Ce DataPoint est identifié par un label (nom de la variable dans Minecraft) et contient une variable. Chaque DataPoint est ensuite référencé dans une HashMap. Une méthode de la classe DataPoint, "PushValueToConnector", permet d'actualiser la valeur d'un DataPoint sur toutes les plateformes liées au code Java.

- **BooleanDataPoint**

    => Variante de type Boolean du DataPoint

- **FloatDataPoint**

    => Variante de type Float du DataPoint

- **DatabaseConnector**

    => Cette classe permet de transmettre des données à la Database (InfluxDB). Des adaptations sur l'écoulement du temps sont aussi effectuées pour adapter la vitesse de Minecraft à celle de la réalité (1 jour = 10 minutes).

- **FieldConnector**

    => Permet d'actualiser les valeurs de tous les DataPoints. C'est cette classe qui scanne les valeurs de toutes nos variables via le "ModbusAccessor", puis actualise les valeurs des DataPoint.

- **BooleanRegister**

    => La classe BooleanRegister est une classe qui contient une table de BooleanDataPoint liés à leurs registres. Elle permet de lire et d'écrire des données de type bool

- **FloatRegister**

  => La classe FloatRegister est une classe qui contient une table de FloatDataPoint liés a leurs registres. Elle permet de lire et d'écrire des données de type float à partir du Modbus.


- **ModbusAccessor**

  => La classe ModbusAccessor permet de créer une connexion avec le monde Minecraft via Modbus et de lire et écrire des valeurs via ce protocole pour acquérir ou modifier les variables dans le GridMinecraft.


- **PollTask**

  => La classe PollTask hérite de la classe TimerTask et permet de lire périodiquement toutes les valeurs des DataPoints pour les actualiser.


- **WebConnector**

  => La classe WebConnector est responsable de la connexion et de la gestion des données entre le GridMinecraft et une interface Web. Il gère l'envoi et la réception de données avec l'interface web et utilise la bibliothèque Java-WebSocket pour la gestion des connexions WebSocket.

## Comment lancer le jeu :

- Démarrer d'abord le monde Minecraft
- Pour lancer le monde, dans votre terminal, allez sur "ElectricalAge-develop" et entrez la commande suivante :  .\gradlew runClient,
- Pour lancer MinecraftController, dans votre terminal, entrez la commande suivante : `java -jar Minecraft.jar https://influx.sdi.hevs.ch SIn05 SIn05 localhost 1502`
Cette structure de message n'est pas la meme que celle demandee dans Cyberlearn, mais respectez le code donne au debut du projet.

## Liens
[JavaDoc](javadoc/hei/index.html)

[JarFile](out/artifacts/Minecraft_jar/Minecraft.jar)

[CSVFile](ModbusMap.csv)

[DataBase](https://grafana.sdi.hevs.ch/)

[InfluxDB](https://influx.sdi.hevs.ch/orgs/5de694672d3f2464)

[WebConnector](WebClient/index.html)