package ch.hevs.isi.web;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.util.ArrayList;

/**
 * Permet la connexion et la gestion du GridMinecraft avec une interface Web.
 * Gère l'envoie et la réception de data avec l'interface web.
 */
public class WebConnector implements DataPointListener {

    /**
     * Attributs de la classe WebConnector
     */
    private static WebConnector instance = null;
    WebSocketServer wss = null;
    private ArrayList<WebSocket> webSockets; // Tableau (liste) de WebSocket (Permet de gérer plusieurs Websockets en même temps)

    /**
     * Constructeur de WebConnector
     * Créer le WebSocketServer et setup la connexion avec notre interface web
     */
    public WebConnector() {
        wss = new WebSocketServer(new InetSocketAddress(8888)) {
            @Override
            public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
                // Lorsque la comm. avec le server est ouverte, ce bout de code s'exécute
                // Ajoute un Websocket dans notre liste
                webSockets.add(webSocket);
                //Envoie dans le Web socket un "welcome message"
                webSocket.send("Welcome chez nous");
            }
            @Override
            public void onClose(WebSocket webSocket, int i, String s, boolean b) {
                webSockets.remove(webSocket);
            }
            @Override
            public void onMessage(WebSocket webSocket, String s) {

                String[] message = s.split("=");
                DataPoint dp = DataPoint.getDataPointFromLabel(message[0]);

                //Détecte si le datapoint est un float ou un boolean et set la nouvelle valeur du datapoint
                if(dp.getClass() == FloatDataPoint.class){
                   ((FloatDataPoint) dp).setValue(Float.valueOf(message[1]));
                } else if (dp.getClass() == BooleanDataPoint.class) {
                    ((BooleanDataPoint) dp).setValue(Boolean.parseBoolean(message[1]));
                }
                webSocket.send(s);
            }
            @Override
            public void onError(WebSocket webSocket, Exception e) {
                System.out.println(e.getStackTrace());
            }
            @Override
            public void onStart() {
            }
        };
        wss.start();

    }

    /**
     * Renvoie une référence au singleton.
     * Elle crée l'objet "instance" unique s'il n'existe pas.
     * @return
     */
    public static WebConnector getInstance() {
        if (instance == null)
            instance = new WebConnector();
        return instance;
    }

    /**
     * Envoie au WebSocket le label et sa valeur passé en argument
     * @param label
     * @param value
     */
    private void pushToWeb(String label, String value) {
        //System.out.print("WB : " + "label = " + label + ", value = " + value)
        for(WebSocket ws : wss.getConnections()){
            ws.send(label +"="+ value);
        }

    }
    /**
     * Exécute pushToWeb pour actualiser la nouvelle valeur du DataPoint
     * @param dp
     */
    public void onNewValue(DataPoint dp){
        pushToWeb(dp.getLabel(), dp.getValue());
    }
}
