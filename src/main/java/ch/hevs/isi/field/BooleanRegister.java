package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;

import java.util.HashMap;

/**
 * Classe BooleanRegister
 * contient une table contenant tout les DataPoint booléen lié avec leurs registres
 */
public class BooleanRegister {

    /**
     * Création d'une MAP qui stock un BooleanDataPoint Lié avec son registre
     */
    static private HashMap<BooleanDataPoint, BooleanRegister> boolHM = new HashMap<>();


    /**
     * Attributs de la classe Boolean register
     * contient la valeur du dp, son addresse, et le dp
     */
    private boolean value;
    private int address;
    private BooleanDataPoint bdataPoint;

    private ModbusAccessor ma = ModbusAccessor.getInstance();

    /**
     * Constructeur de BooleanDataPoint
     * à chaque nouvelle instance de cette classe, l'objet est stocké dans la map et est lié à son registre
     * @param bdp
     * @param address
     */
    public BooleanRegister(BooleanDataPoint bdp, int address){
        this.address = address;
        this.bdataPoint = bdp;
        // Enregistre automatiquement le nouvel objet dans la dataPointMap
        boolHM.put(bdp,this);
    }

    /**
     * Cherche dans la HashMap le registre lié au boolean data point appelé par la méthode
     * @param dp
     * @return le registre du BDP passé en paramètre
     */
    public static BooleanRegister getRegisterFromDataPoint(DataPoint dp){
        return boolHM.get(dp);
    }

    /**
     * Lit et actualise tous les BooleanDataPoint en fonction
     * des valeurs récupérées grâce au ModbusAccessor
     */
    public static void poll() {

        for (BooleanRegister br : boolHM.values()) {
            br.read();
        }
    }

    /**
     * la valeur du field dans le DataPoint correspondant est actualisée
     */
   public void read(){bdataPoint.setValue(ma.readBoolean(address));}

    /**
     * la valeur actuel du field dans le DataPoint correspondant est écrit sur le Modbus
     */
   public void write(){
       ma.writeBoolean(address,bdataPoint.getBooleanValue());
   }
}
