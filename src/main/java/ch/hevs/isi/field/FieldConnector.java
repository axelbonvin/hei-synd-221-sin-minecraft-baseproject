package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;

/**
 * Permet l'actualisation des valeurs de tous les Datapoint
 */
public class FieldConnector implements DataPointListener {
    private static FieldConnector instance = null;

    /**
     *   Renvoie une référence au singleton.
     *   Elle crée l'objet "instance" unique s'il n'existe pas.
     *
     *   @return instance
     */
    public static FieldConnector getInstance(){
        if (instance == null)
            instance = new FieldConnector();
        return instance;
    }

    /**
     * Ecrit une nouvelle valeur dans le registre du dataPoint corespondant.
     * Cette méthode différentie si un float ou un boolean data point est passé en paramètre
     * @param dp
     */
    public void onNewValue(DataPoint dp){
        if(dp instanceof BooleanDataPoint){
            BooleanRegister.getRegisterFromDataPoint(dp).write();

        } else if (dp instanceof FloatDataPoint) {
            FloatRegister.getRegisterFromDataPoint(dp).write();

        } else {
            System.out.println("Error with DataPoint");
        }
    }

    /**
     * Permet de lire toutes les variables Float et Boolean via le ModbusAccessor.
     * Ensuite, les valeurs de nos DataPoint correspondantes sont actualisées
     */
    public void readAllValue(){
        FloatRegister.poll();
        BooleanRegister.poll();
    }
}
