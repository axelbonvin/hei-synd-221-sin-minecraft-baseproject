package ch.hevs.isi.field;

import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.ip.tcp.TcpMaster;
import com.serotonin.modbus4j.locator.BaseLocator;

/**
 * Crée la connexion avec le jeu Minecraft et s'occupe de lire et écrire les valeurs sur le protocole Modbus
 */
public class ModbusAccessor {

    /**
     * Attributs de la classe ModbusAccessor
     */
    static ModbusAccessor MB;
    private TcpMaster master;

    /**
     * Renvoie une référence au singleton.
     * Elle crée l'objet DC unique s'il n'existe pas.
     */
    public static ModbusAccessor getInstance() {
        if (MB == null) {
            MB = new ModbusAccessor();
        }
        return MB;
    }

    // Main de test pour tester la communication
//    public static void main(String[] args) throws ModbusInitException  {
//
//        ModbusAccessor ma = ModbusAccessor.getInstance();
//        ma.connect("localhost", 1502);
//        System.out.println(ma.readBoolean(609));
//    }

    /**
     * Connecte l'ipAddress au Modbus server et qui garde la connection ouverte
     * @param ipAddress
     * @param port
     */
    public void connect(String ipAddress, int port) {
        try {
        IpParameters params = new IpParameters();
        params.setHost(ipAddress);
        params.setPort(port);
        master = (TcpMaster) new ModbusFactory().createTcpMaster(params, true);
        master.init();
        }
        catch (ModbusInitException e){
            System.out.println("Modbus error : connection failed");
        }
    }

    /**
     *  Lit une valeur float du registre sélectionné
     * @param regAddress
     * @return la val en float du registre lu
     */
    public Float readFloat(int regAddress) {
        try {
            return (Float) master.getValue(BaseLocator.inputRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT));
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *  Ecrit une valeur float dans le registre sélectionné
     * @param newValue
     * @param regAddress
     */
    public void writeFloat(int regAddress, float newValue) {
        try {
            master.setValue(BaseLocator.holdingRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT), newValue);
        } catch (ModbusTransportException e) {
            throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Lit une valeur boolean dans le registre sélectionné
     * @param regAddress
     * @return la val boolean du registre lu
     */
    public Boolean readBoolean(int regAddress) {
        try {
            return master.getValue(BaseLocator.coilStatus(1, regAddress));
        } catch (ModbusTransportException e) {
            throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *  Ecrit une valeur boolean dans le registre sélectionné
     * @param  regAddress
     * @param newValue
     */
    public void writeBoolean(int regAddress, boolean newValue) {
        try {
            master.setValue(BaseLocator.coilStatus(1, regAddress), newValue);
        } catch (ModbusTransportException e) {
            throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            throw new RuntimeException(e);
        }
    }
}
