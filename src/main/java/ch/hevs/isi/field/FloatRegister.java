package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.HashMap;

/**
 * Classe FloatRegister
 * contient une table contenant tout les float DataPoints liés avec leurs registres
 */
public class FloatRegister {

    /**
     * Création d'une MAP qui stock un FloatDataPoint Lié avec son registre
     */
    static private HashMap<FloatDataPoint, FloatRegister> floatHM = new HashMap<>();

    /**
     * Attributs de la class float register
     * contient la valeur du dp, son addresse, et le dp, un offset de la valeur et sa pente
     */
    int range;
    int offset;
    private float value;
    private int address;
    private FloatDataPoint fdataPoint;

    private ModbusAccessor ma = ModbusAccessor.getInstance();

    /**
     * Constructeur de FloatRegister
     * à chaque nouvelle instance de cette classe, l'objet est stocké dans la map et est lié à un registre
     */
    public FloatRegister(FloatDataPoint fdp, int address, int range, int offset){
        this.address = address;
        this.fdataPoint = fdp;

        this.range = range;
        this.offset = offset;
        // Enregistre automatiquement le nouvel objet dans la dataPointMap
        floatHM.put(fdp,this);
    }

    /**
     * Cherche dans la HashMap le registre lié au float data point appelé par la méthode
     * @param dp
     * @return le registre du fDP passé en paramètre
     */
    public static FloatRegister getRegisterFromDataPoint(DataPoint dp){
        return floatHM.get(dp);
    }

    /**
     * Lit et actualise tous les FloatDataPoint en fonction
     * des valeurs récupérées grâce au ModbusAccessor
     */
    public static void poll() {
        for (FloatRegister fr : floatHM.values()) {
            fr.read();
        }
    }

    /**
     * la valeur du field dans le DataPoint correspondant est actualisée
     */
    public void read(){
        Float val = offset + range * ma.readFloat(address);
        if (val != null)
            fdataPoint.setValue(val);
    }

    /**
     * la valeur actuel du field dans le DataPoint correspondant est écrit sur le Modbus
     */
    public void write(){
        ma.writeFloat(address,(fdataPoint.getFloatValue()-offset)/range );
    }
}
