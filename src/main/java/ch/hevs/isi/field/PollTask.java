package ch.hevs.isi.field;

import java.util.TimerTask;

/**
 *  Permet de lire périodiquement toutes les valeurs de nos variables, pour les actualiser.
 */
public class PollTask extends TimerTask {

    /**
     * lit toutes les valeurs des DataPoints
     */
    @Override
    public void run() {
        FieldConnector.getInstance().readAllValue();
    }
}

