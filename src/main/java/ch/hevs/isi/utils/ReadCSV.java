package ch.hevs.isi.utils;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.field.BooleanRegister;
import ch.hevs.isi.field.FloatRegister;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Lit les valeurs du fichier CSV contenant toutes les infos sur les DataPoints
 */
public class ReadCSV {
    /**
     * Lit les données d'un fichier, le ModbusMap dans lequel est stocké la liste de nos variables Minecraft
     * et leurs caractéristiques.
     * Ajout des variables de Minecraft comme DataPoint dans la HashMap.
     */
    public static void importVarFromCSV(){
        BufferedReader br = Utility.fileParser(null, "ModbusMap.csv");
        if (br == null)
            System.exit(1);

        try {
            String line = br.readLine();
            while (line != null) {
                String config[] = line.split(";");

                String label = config[0];
                if (!label.equals("Label")) {
                    boolean tFloat = config[1].equalsIgnoreCase("f");
                    boolean isOutput = config[3].equals("Y");
                    int address = Integer.parseInt(config[4]);

                    int range = Integer.valueOf(config[5]);
                    int offset = Integer.valueOf(config[6]);

                    if (tFloat) {
                        new FloatRegister(new FloatDataPoint(label, isOutput), address, range, offset);
                    } else
                        new BooleanRegister(new BooleanDataPoint(label, isOutput), address);
                }
                line = br.readLine();
            }
        } catch(IOException e){
            throw new RuntimeException(e);
        }
    }
}
