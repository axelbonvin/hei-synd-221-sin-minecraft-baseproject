package ch.hevs.isi.core;

/**
 * Extension de la classe DataPoint, de type Boolean.
 * Point d'entrée ou de sortie contenant une valeur en Booléen
 * Un DataPoint permet d'actualiser les modifications d'une variable sur le web, le smartControl et la Database.
 */
public class BooleanDataPoint extends DataPoint{

    /**
     * Attribut de la classe,
     * Valeur booléenne du BooleanDataPoint
     */
    private boolean value;

    /**
     * Constructeur de BooleanDataPoint
     * @param label nom de notre BooleanDataPoint
     * @param isOutput 1 = le datapoint est une sortie ; 0 = le datapoint est une entrée
     */
    public BooleanDataPoint(String label, Boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Return la valeur de la variable "value" sous forme de String.
     * @return (String) value
     */
    public String getValue() {
        return String.valueOf(value);
    }

    /**
     * Remplace la value par la valeur du paramètre d'entrée
     * @param value
     */
    public void setValue(boolean value) {
        this.value = value;
        pushValueToConnectors();
    }

    /**
     * Return la valeur de la variable "value" sous forme de boolean.
     * @return (boolean) value
     */
    public Boolean getBooleanValue(){
        return value;
    }

}
