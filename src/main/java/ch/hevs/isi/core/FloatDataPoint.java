package ch.hevs.isi.core;

/**
 * Extension de la classe DataPoint, de type Float.
 * Point d'entrée ou de sortie contenant une valeur en float.
 * Un DataPoint permet d'actualiser les modifications d'une variable sur le web, le smartControl et la Database.
 */
public class FloatDataPoint extends DataPoint{

    /**
     * Attribut de la classe FloatDatapoint
     * Valeur en float du DataPoint
     */
    private float value;

    /**
     * Constructeur de FloatDataPoint
     * @param label
     * @param isOutput
     */
    public FloatDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Return la valeur de la variable "value" sous forme de String.
     * @return (String) value
     */
    public String getValue() {
        return String.valueOf(value);
    }

    /**
     * Remplace la value par la valeur du paramètre d'entrée
     * @param value
     */
    public void setValue(float value) {
        this.value = value;
        pushValueToConnectors();
    }

    /**
     * Return la valeur de la variable "value" sous forme de float.
     * @return (float) value
     */
    public float getFloatValue(){
        return value;
    }

}
