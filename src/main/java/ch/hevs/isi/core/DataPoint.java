package ch.hevs.isi.core;

import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;
import java.util.HashMap;

/**
 *  Permet d'actualiser les modifications d'une variable sur le web, le smartControl et la Database.
 *  Permet d'identifier une variable en fonction de son label.
 */
public abstract class DataPoint {

    /**
     * Attributs de la classe DataPoint,
     * - Label est le nom donné au datapoint
     * - isOutput définit si le datapoint est une entrée = 0, ou une sortie = 1,
     * - dataPointMap est une table contenant tout les Datapoint lié avec leurs Labels
     */
    static private HashMap<String, DataPoint> dataPointMap  = new HashMap<>();
    private String label;
    private boolean isOutput;

    /**
     * Constructeur de la class DataPoint.
     * Crée un nouveau DataPoint, et l'enregistre dans la HashMap lié avec son label
     * @param label
     * @param isOutput
     */
    protected DataPoint(String label, boolean isOutput){
        this.label = label;
        this.isOutput = isOutput;

        // Enregistre automatiquement le nouvel objet dans la dataPointMap
        dataPointMap.put(label, this);
    }

    /**
     * Permet d'obtenir le dataPoint correspondant du Label passé en paramètre.
     * @param label
     * @return DataPoint
     */
    public static DataPoint getDataPointFromLabel (String label){
        return dataPointMap.get(label);
    }

    /**
     * Return la valeur de la variable "value" sous forme de String.
     * @return (String) value
     */
    public abstract String getValue();

    /**
     * Actualise les valeurs du DataPoint dans les différents connecteurs : DataBase, WebConnector et FieldConnector
     */
    protected void pushValueToConnectors() {
        DatabaseConnector.getInstance().onNewValue(this);
        WebConnector.getInstance().onNewValue(this);
        if (isOutput) {
            FieldConnector.getInstance().onNewValue(this);
        }
    }

    /**
     * Return le label du DataPoint
     * @return label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Permet de savoir si le DataPoint est une output ou une input
     *  isOutput = 1 => le DataPoint est une output.
     * @return isOutput
     */
    public Boolean getisOutput(){
        return isOutput;
    }
}
