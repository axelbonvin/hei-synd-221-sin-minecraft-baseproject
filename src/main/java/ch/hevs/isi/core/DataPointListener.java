package ch.hevs.isi.core;

/**
 * Classe interface
 * force les classes DatabaseConnector, FieldConnector, WebConnector à avoir une méthode onNewValue
 */
public interface DataPointListener {

    /**
     * La méthode doit permettre d'actualiser une nouvelle valeur dans les 3 endroits implémentés
     * @param dp
     */
    void onNewValue(DataPoint dp);
}
