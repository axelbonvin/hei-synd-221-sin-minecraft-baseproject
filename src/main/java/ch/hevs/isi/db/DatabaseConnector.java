package ch.hevs.isi.db;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *  Permet d'accéder à la DataBase (influx DB) et de communiquer avec afin de monitorer les valeurs sur Grafana.
 */
public class DatabaseConnector implements DataPointListener{

    /**
     * Atrributs de la classe DatabaseConnector
     * - backDays sont les jours passés
     * - _timestamp est un marquage temporel des valeurs
     */
    private static DatabaseConnector instance = null;
    int backDays = 4;
    TimeManager _timeManager = new TimeManager(backDays);
    URL url;
    String token;
    private long _timestamp=0;

    /**
     * Renvoie une référence au singleton.
     * Elle crée l'objet instance unique s'il n'existe pas
     * @return instance
     */
    public static DatabaseConnector getInstance(){
        if (instance == null)
            instance = new DatabaseConnector();
        return instance;
    }


    /**
     * Permet de configurer la connection avec la base de donnée avec les données de bases
     * @param baseUrl
     * @param organisation
     * @param bucket
     * @param token
     * @throws MalformedURLException
     */
    public void configure(String baseUrl, String organisation, String bucket, String token) throws MalformedURLException {
        url = new URL(baseUrl + "/api/v2/write?org=" + organisation + "&bucket=" + bucket);
        this.token = token;
    }




    /**
     * Permet de créer la connexion avec notre DataBase Influx.
     * Elle crée ensuite une nouvelle entrée dans la DB et envoie la valeur de notre variable dans la DB pour stockage.
     * @param label
     * @param value
     */
   private void pushToDatabase(String label, String value){
       try {
           URL url = new URL("https://influx.sdi.hevs.ch/api/v2/write?org=SIn05&bucket=SIn05");

           HttpURLConnection connection = (HttpURLConnection) url.openConnection();
               connection.setRequestProperty ("Authorization", "Token " + "bW6-s_EkTkZ-rT-Mj0kt1uZQYOPKio5FdEK-OtwWJlxDwYQ5OkH599t1jUz4VV47SrkKonLDHB0jRJ3Eu6OUTA==");

           connection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
           connection.setRequestProperty("Accept", "application/json");

           connection.setRequestMethod("POST");
           connection.setDoOutput(true);

           OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
           writer.write("Minecraft "+ label + "=" +value+ " "+_timestamp);
//           writer.write(label+",type=Measure field1="+value+ " "+_timestamp);
           writer.flush();
           int responseCode = connection.getResponseCode();
           //System.out.println(responseCode); // ligne d'affichage du code de réponse pour dépanner

       } catch (IOException e){
           System.out.println("Connection to db failed chehh");
       }
   }

    /**
     * Permet d'ajouter la valeur du datapoint dans notre database (Influx)
     * @param dp
     */
   public void onNewValue(DataPoint dp){
      if (dp.getLabel().equals("CLOCK_FLOAT")) {
          _timeManager.setTimestamp(dp.getValue());
          _timestamp = _timeManager.getNanosForDB();
       }
      if (_timestamp != 0){
          pushToDatabase(dp.getLabel(), dp.getValue());
      }
   }

   }
