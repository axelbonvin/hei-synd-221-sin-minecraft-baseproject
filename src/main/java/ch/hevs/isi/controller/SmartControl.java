package ch.hevs.isi.controller;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.TimerTask;

/**
 * Gère la gestion du réseau du GridMinecraft.
 * Maximise la production de l'usine, tout en assurant la stabilité du réseau.
 * Minimise la consommation de charbon.
 */
public class SmartControl extends TimerTask {
    /**
     * Attributs de la classe SmartControl,
     * DataPoint que nous avons besoin pour réguler notre Grid
     */
    FloatDataPoint battChrg = (FloatDataPoint) DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
    FloatDataPoint solarP = (FloatDataPoint) DataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");
    FloatDataPoint windP = (FloatDataPoint) DataPoint.getDataPointFromLabel("WIND_P_FLOAT");
    FloatDataPoint coalP = (FloatDataPoint) DataPoint.getDataPointFromLabel("COAL_P_FLOAT");
    FloatDataPoint homeP = (FloatDataPoint) DataPoint.getDataPointFromLabel("HOME_P_FLOAT");
    FloatDataPoint publicP = (FloatDataPoint) DataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT");
    FloatDataPoint bunkerP = (FloatDataPoint) DataPoint.getDataPointFromLabel("BUNKER_P_FLOAT");
    FloatDataPoint remoteFactory  = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
    FloatDataPoint remoteCoal  = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
    BooleanDataPoint remotePV = (BooleanDataPoint) DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
    BooleanDataPoint remoteWind = (BooleanDataPoint) DataPoint.getDataPointFromLabel("REMOTE_WIND_SW");

    /**
     * Détermine les différents états de charge de la batterie.
     * 4 = Surchage réseau ! Emergency State
     * 3 = Batterie charge critique => trop pleine
     * 2 = Batterie dans sa capacité optimale
     * 1 = Batterie légèrement faible
     * 0 = Batterie charge faible critique
     * @return le chiffre correspondant à l'état de la batterie
     */
    public int batteryState(){
        // Surchage réseau ! Emergency State
        if (battChrg.getFloatValue()>=1){
            return 4;
        }
        // Batterie charge critique => trop pleine
        if (battChrg.getFloatValue()<1 && battChrg.getFloatValue()>=0.9){
            return 3;
        }
        // Batterie dans sa capacité optimale
        if(battChrg.getFloatValue()<0.9 && battChrg.getFloatValue()>=0.55) {
            return 2;
        }
        // Batterie légèrement faible
        if (battChrg.getFloatValue()<0.55 && battChrg.getFloatValue()>=0.4){
            return 1;
        }
        // Batterie charge faible critique
        if (battChrg.getFloatValue()<0.4){
            return 0;
        }
        else return -1;
    }

    /**
     * Commande les différentes installations du GridMinecraft, en fonction de l'indication de charge de la batterie : batteryState().
     */
    public void controlloop(){
        float prodTot = solarP.getFloatValue()+ windP.getFloatValue()+ coalP.getFloatValue();
        float prodVerte = solarP.getFloatValue()+windP.getFloatValue();
        float consoTotFixe = homeP.getFloatValue()+publicP.getFloatValue()+bunkerP.getFloatValue();

        switch (batteryState()) {
            case 4:
                remoteFactory.setValue(1f);
                remoteCoal.setValue(0f);

                remotePV.setValue(false);
                remoteWind.setValue(false);
                break;

            case 3:
                remoteFactory.setValue(1f);
                remoteCoal.setValue(0f);

                remotePV.setValue(true);
                remoteWind.setValue(true);
                break;
            case 2:
                remotePV.setValue(true);
                remoteWind.setValue(true);
                // si la consigne dépasse 100% on la bride au max
                if((prodVerte-consoTotFixe)/1000 > 1){
                    remoteFactory.setValue(1); // 100% de valeur de consigne
                    //sinon on met en pourcent la différence entre la production verte et la consomation
                }else {
                    remoteFactory.setValue((prodVerte-consoTotFixe)/1000); // division par 1000 d'une valeur en watt pour avoir une val entre 1 et 0
                }


                remoteCoal.setValue(0f);
                break;

            case 1:
                remotePV.setValue(true);
                remoteWind.setValue(true);

                remoteFactory.setValue(0.1f);
                remoteCoal.setValue(0.6f);
                break;

            case 0:
                remotePV.setValue(true);
                remoteWind.setValue(true);

                remoteFactory.setValue(0f);
                remoteCoal.setValue(1f);
                break;
        }

    }
    @Override
    public void run() {controlloop();}
}
